﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BooksEFCore.Models;
using Microsoft.AspNetCore.Mvc;

namespace BooksEFCore.Controllers
{
    public class AuthorController : Controller
    {
        private IAuthorRepository authorRepo;

        public AuthorController(IAuthorRepository authorRepo)
        {
            this.authorRepo = authorRepo;
        }

        public IActionResult List()
        {
            return View(authorRepo.Authors
                .OrderBy(a => a.LastName)
                .ThenBy(a => a.FirstName));
        }
    }
}