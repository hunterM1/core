﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BooksEFCore.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BooksEFCore.Controllers
{
    public class TitleController : Controller
    {
        private ITitleRepository titleRepo;

        public TitleController(ITitleRepository titleRepo)
        {
            this.titleRepo = titleRepo;
        }

        //Filter to just titles with sales > 100,000 and price > 10
        //public IActionResult List() => View(titleRepo.Titles.Where(t => t.Sales > 100000 && t.Price > 10));

        public IActionResult List()
        {
            IEnumerable<Title> fTitles = titleRepo.Titles
                .Where(t => t.Sales > 100000 && t.Price > 10)
                .OrderByDescending(t => t.Sales);

            return View(fTitles);
        }
    }
}
