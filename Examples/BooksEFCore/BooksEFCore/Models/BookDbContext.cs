﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksEFCore.Models
{
    public class BookDbContext : DbContext
    {
        public BookDbContext(DbContextOptions<BookDbContext> options) : base(options)
        {

        }

        public DbSet<Publisher> Publishers { get; set; }

        public DbSet<Title> Titles { get; set; }

        public DbSet<Author> Authors { get; set; }

        public DbSet<TitleAuthor> TitleAuthors { get; set; }
    }
}
