﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksEFCore.Models
{
    public class EFTitleRepository : ITitleRepository
    {
        private BookDbContext context;

        public IEnumerable<Title> Titles => context.Titles;

        public EFTitleRepository(BookDbContext context)
        {
            this.context = context;
        }
    }
}
