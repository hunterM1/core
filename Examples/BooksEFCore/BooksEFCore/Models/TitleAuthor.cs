﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksEFCore.Models
{
    public class TitleAuthor
    {
        public int TitleAuthorID { get; set; }

        public int AuthorOrder { get; set; }

        //associations
        public int AuthorID { get; set; }

        public Author Author { get; set; }

        public int TitleID { get; set; }

        public Title Title { get; set; }
    }
}
