﻿using System.Collections.Generic;

namespace BooksEFCore.Models
{
    public interface ITitleRepository
    {
        IEnumerable<Title> Titles { get; }
    }
}