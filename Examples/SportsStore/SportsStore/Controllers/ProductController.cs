﻿using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using SportsStore.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository prodRepo;
        private int pageSize = 4;
        public ProductController(IProductRepository prodRepo)
        {
            this.prodRepo = prodRepo;
        }

        /* without pagination links
        public IActionResult List(int page = 1)
        {
            IEnumerable<Product> fProd = prodRepo.Products
                .OrderBy(p => p.Name)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            return View(fProd);
        }
        */

        //with pagination links
        public IActionResult List(int page = 1)
        {
            IEnumerable<Product> fProd = prodRepo.Products
                .OrderBy(p => p.Name)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            PagingInfo pageInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = prodRepo.Products.Count()
            };

            ProductsListViewModel vModel = new ProductsListViewModel()
            {
                Products = fProd,
                PagingInfo = pageInfo
            };

            return View(vModel);
        }
    }
}
