﻿using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Controllers
{
    public class AdminController : Controller
    {
        private IProductRepository prodRepo;

        public AdminController(IProductRepository prodRepo)
        {
            this.prodRepo = prodRepo;
        }
        public IActionResult Index()
        {
            return View(prodRepo.Products);
        }

        public IActionResult Edit(int productId)
        {
            return View(prodRepo.Products.FirstOrDefault(p => p.ProductID == productId));
        }

        [HttpPost]
        public IActionResult Edit(Product product)
        {
            if (ModelState.IsValid)
            {
                prodRepo.SaveProduct(product);
                TempData["message"] = $"{product.Name} has been saved";
                return RedirectToAction("Index");
            }
            else
            {
                return View(product);
            }
        }

        public IActionResult Create()
        {
            return View("Edit", new Product());
        }

        [HttpPost]
        public IActionResult Delete(int productId)
        {
            Product delProd = prodRepo.DeleteProduct(productId);

            if(delProd != null)
            {
                TempData["message"] = $"ID: {delProd.ProductID} {delProd.Name} was deleted";
            }

            return RedirectToAction("Index");
        }
    }
}
