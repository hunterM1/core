﻿using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using SportsStore.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository prodRepo;
        private int pageSize = 4;
        public ProductController(IProductRepository prodRepo)
        {
            this.prodRepo = prodRepo;
        }

        /* without pagination links
        public IActionResult List(int page = 1)
        {
            IEnumerable<Product> fProd = prodRepo.Products
                .OrderBy(p => p.Name)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            return View(fProd);
        }
        */

        //with pagination links
        /*
        public IActionResult List(string category, int page = 1) => View(new ProductsListViewModel
        {
            Products = prodRepo.Products
                .Where(p => category == null || p.Category == category)
                .OrderBy(p => p.ProductID)
                .Skip((page - 1) * pageSize)
                .Take(pageSize),
            PagingInfo = new PagingInfo
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = prodRepo.Products.Count()
            },
            CurrentCategory = category
        });
        */

        //break it down some...
        public IActionResult List(string category, int page = 1)
        {
            IEnumerable<Product> fProd = prodRepo.Products
                .Where(p => category == null || p.Category == category)
                .OrderBy(p => p.Name)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            PagingInfo pageInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                //TotalItems = prodRepo.Products.Count() this had the total count of Product items. Re-factor to get total items per category for pagination
                TotalItems = category == null ? prodRepo.Products.Count() : prodRepo.Products.Where(p => p.Category == category).Count()
            };

            ProductsListViewModel vModel = new ProductsListViewModel()
            {
                Products = fProd,
                PagingInfo = pageInfo,
                CurrentCategory = category
            };

            return View(vModel);
        }
    }
}
