﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Models
{
    public class EFProductRepository : IProductRepository
    {
        private ApplicationDbContext context;

        public IEnumerable<Product> Products => context.Products;

        public EFProductRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public void SaveProduct(Product product)
        {
            if(product.ProductID == 0)
            {
                //add new product
                context.Products.Add(product);
            }
            else
            {
                //update an existing product
                Product dbProd = context.Products.FirstOrDefault(p => p.ProductID == product.ProductID);
                if(dbProd != null)
                {
                    dbProd.Name = product.Name;
                    dbProd.Description = product.Description;
                    dbProd.Price = product.Price;
                    dbProd.Category = product.Category;
                }
            }

            context.SaveChanges();
        }

        public Product DeleteProduct(int productId)
        {
            Product efProd = context.Products.FirstOrDefault(p => p.ProductID == productId);
            if(efProd != null)
            {
                context.Products.Remove(efProd);
                context.SaveChanges();
            }

            return efProd;
        }
    }
}
