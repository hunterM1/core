﻿using Microsoft.AspNetCore.Mvc;
using SportsStore.Infrastructure;
using SportsStore.Models;
using SportsStore.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Controllers
{
    public class CartController : Controller
    {
        private IProductRepository prodRepo;
        private Cart cart;
        
        public CartController(IProductRepository prodRepo, Cart cartService)
        {
            this.prodRepo = prodRepo;
            cart = cartService;
        }

        public RedirectToActionResult AddToCart(int productId, string ReturnURL)
        {
            Product prod = prodRepo.Products.FirstOrDefault(p => p.ProductID == productId);

            if(prod != null)
            {
                cart.AddItem(prod, 1);
            }

            return RedirectToAction("Index", new { ReturnURL });
        }

        public RedirectToActionResult RemoveFromCart(int productId, string returnURL)
        {
            Product prod = prodRepo.Products.FirstOrDefault(p => p.ProductID == productId);

            if(prod != null)
            {
                cart.RemoveLine(prod);
            }

            return RedirectToAction("Index", new { returnURL });
        }

        public IActionResult Index(string returnURL)
        {
            CartIndexViewModel model = new CartIndexViewModel()
            {
                Cart = cart,
                ReturnUrl = returnURL
            };

            return View(model);
        }
    }
}
