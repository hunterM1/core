﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Controllers
{
    public class OrderController : Controller
    {
        private IOrderRepository orderRepo;
        private Cart cart;

        public OrderController(IOrderRepository orderRepo, Cart cart)
        {
            this.orderRepo = orderRepo;
            this.cart = cart;
        }

        public IActionResult Checkout() => View(new Order());

        [HttpPost]
        public IActionResult Checkout(Order order)
        {
            if(cart.Lines.Count() == 0)
            {
                ModelState.AddModelError("", "Sorry, your cart is empty!");
            }

            if (ModelState.IsValid)
            {
                order.Lines = cart.Lines.ToArray();
                orderRepo.SaveOrder(order);
                return RedirectToAction("Completed");
            }
            else
            {
                return View(order);
            }
        }

        public IActionResult Completed()
        {
            cart.Clear();
            return View();
        }

        [Authorize]
        public IActionResult List() => View(orderRepo.Orders.Where(o => !o.Shipped));

        [HttpPost]
        [Authorize]
        public IActionResult MarkShipped(int orderId)
        {
            Order order = orderRepo.Orders.FirstOrDefault(o => o.OrderID == orderId);

            if(order != null)
            {
                order.Shipped = true;
                orderRepo.SaveOrder(order);
            }

            return RedirectToAction("List");
        }
    }
}
