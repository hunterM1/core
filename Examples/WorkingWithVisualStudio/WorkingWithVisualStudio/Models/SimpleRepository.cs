﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkingWithVisualStudio.Models
{
    public class SimpleRepository : IRepository
    {
        private static SimpleRepository sharedRepository = new SimpleRepository();
        private Dictionary<string, Product> products = new Dictionary<string, Product>();

        //a lambda expression, as a short-hand property invocation
        public static SimpleRepository SharedRepository => sharedRepository;

        public SimpleRepository()
        {
            Product[] products = new Product[]
            {
                new Product() {Name = "Kayak", Price=275M},
                new Product() {Name = "Lifejacket", Price=48.95M},
                new Product() {Name="Soccer ball", Price=19.50M},
                new Product() {Name="Corner flag", Price=34.95M}
            };

            foreach(Product p in products)
            {
                AddProduct(p);
            }

        }

        public IEnumerable<Product> Products => products.Values;

        //recall we can use a lambda expression here since it is a one liner
        public void AddProduct(Product p) => products.Add(p.Name, p);
    }
}
