﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using WorkingWithVisualStudio.Models;
using WorkingWithVisualStudio.Controllers;
using Xunit;
using System.Linq;

namespace WorkingWithVisualStudio.Tests
{
    public class HomeControllerTest
    {
        class ModelCompleteFakeRepository : IRepository
        {
            public IEnumerable<Product> Products { get; } = new Product[]
            {
                new Product() {Name="P1", Price=275M},
                new Product() {Name="P2", Price=48.95M},
                new Product() {Name="P3", Price=19.50M},
                new Product() {Name="P3", Price=34.95M}
            };

            public void AddProduct(Product p)
            {
                //do nothing, since not required for our test
                throw new NotImplementedException();
            }
        }

        [Fact]
        public void IndexActionModelIsComplete()
        {
            //arrange
            HomeController controller = new HomeController();

            //set the controller to use our fake repo
            controller.Repository = new ModelCompleteFakeRepository();

            //act
            /*Get our first set from the controller
             * we break this up a bit more than the author to make it clearer
             * The controller's index method returns an IActionResult,
             * but we need to cast ViewResult to get at the model
             */
            ViewResult result = (ViewResult)controller.Index();

            //now we have access to the model data in the ViewResult object
            IEnumerable<Product> modelProducts = (IEnumerable<Product>)result?.ViewData.Model;

            //get our second set from the static repo..
            //no need to do this since we set the Products in above class. (This would be the first argument in Assert.Equal below)
            //IEnumerable<Product> repoProducts = SimpleRepository.SharedRepository.Products;

            //assert
            /* We now have our two sets
             * we can use an overload of Assert.Equal to compare the two sets
             * the overload we want accepts an IEqualityComparer
             * which will compare the sets via a lambda
             */

            Assert.Equal(controller.Repository.Products, modelProducts,
                Comparer.Get<Product>((p1, p2) => p1.Name == p2.Name && p1.Price == p2.Price));
        }
    }
}
