﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LanguageFeatures.Models;
using Microsoft.AspNetCore.Mvc;

namespace LanguageFeatures.Controllers
{
    public class HomeController : Controller
    {
        /*
        public ViewResult Index()
        {
            return View(new string[] { "C#", "Language", "Features" });
        }
        */

        /*
        public ViewResult Index()
        {
            List<string> results = new List<string>();

            foreach(Product p in Product.GetProducts())
            {
                string name = p.Name;
                decimal? price = p.Price;
                results.Add(string.Format($"Name: {name}, Price: {price}"));
            }
            return View(results);
        }
        */

        /* Showing subsequent null operator along with conditional operators
        public ViewResult Index()
        {
            List<string> results = new List<string>();

            foreach(Product p in Product.GetProducts())
            {
                string name = p?.Name ?? "<No Name>";
                decimal? price = p?.Price ?? 0;
                string related = p?.Related?.Name ?? "<None>";

                results.Add($"Name: {name}, Price: {price:C2}, Related: {related}");
            }

            return View(results);
        }
        */

        /* Using extension methods
        public ViewResult Index()
        {
            ShoppingCart cart = new ShoppingCart
            {
                Products = Product.GetProducts()
            };

            decimal cartTotal = cart.TotalPrices();

            return View("Index", new string[] { $"Total: {cartTotal:C2}" });
        }
        */

        //Showing more generic extension methods
        /*
        public ViewResult Index()
        {
            ShoppingCart cart = new ShoppingCart
            {
                Products = Product.GetProducts()
            };

            Product[] productArray =
            {
                new Product {Name = "Kayak", Price = 275M},
                new Product {Name = "Lifejacket", Price = 48.95M}
            };

            decimal cartTotal = cart.TotalPrices();
            decimal arrayTotal = productArray.TotalPrices();

            return View("Index", new string[]
            {
                $"Total: {cartTotal:C2}",
                $"Array Total: {arrayTotal:C2}"
            });
        }
        */

        /*
        //Showing filtering extension methods
        public ViewResult Index()
        {
            Product[] productArray =
            {
                new Product {Name = "Kayak", Price = 275M},
                new Product {Name = "Lifejacket", Price = 48.95M},
                new Product {Name = "Soccer Ball", Price = 19.50M},
                new Product {Name = "Corner Flag", Price = 34.95M}
            };

            decimal arrayTotal = productArray.FilterByPrice(20).TotalPrices();
            decimal nameFilterTotal = productArray.FilterbyName('S').TotalPrices();

            return View("Index", new string[]
            {
                $"Array Total: {arrayTotal:C2}",
                $"Name Total: {nameFilterTotal:C2}"
            });
        }
        */

        //Showing filtering extension method using generic Filter extension method found in MyExtensionMethods class
        //We define the delegate functions either explicitly here (FilterByPrice) or inline (nameFilter)

        //define our delegate function for filtering by price (see how this is in the Controller class itself)
        /*
        bool FilterByPrice(Product p)
        {
            return (p?.Price ?? 0) >= 20;
        }
        */
        /*
        public ViewResult Index()
        {
            Product[] productArray =
            {
                new Product {Name = "Kayak", Price = 275M},
                new Product {Name = "Lifejacket", Price = 48.95M},
                new Product {Name = "Soccer Ball", Price = 19.50M},
                new Product {Name = "Corner Flag", Price = 34.95M}
            };

            //define delegate function for filtering by name (see how this is inside the ViewResult method)
            Func<Product, bool> nameFilter = delegate (Product prod)
            {
                return prod?.Name?[0] == 'S';
            };
            */
            //Both of these explicitly stated the filter methods we created in MyExtensionMethods
            //decimal arrayTotal = productArray.FilterByPrice(20).TotalPrices();
            //decimal nameFilterTotal = productArray.FilterbyName('S').TotalPrices();
            /*
            return View("Index", new string[]
            {
                $"Array Total: {arrayTotal:C2}",
                $"Name Total: {nameFilterTotal:C2}"
            });
            */
            /*
            //Both of these are using the generic filter method we created
            decimal priceFilterTotal = productArray.Filter(FilterByPrice).TotalPrices();
            decimal nameFilterTotal = productArray.Filter(nameFilter).TotalPrices();

            return View("Index", new string[]
            {
                $"Price Total: {priceFilterTotal:C2}",
                $"Name Total: {nameFilterTotal:C2}"
            });
        }
        */

        //Show how Lambda expression are awesome
        /*
        public ViewResult Index()
        {
            Product[] productArray =
            {
                new Product {Name = "Kayak", Price = 275M},
                new Product {Name = "Lifejacket", Price = 48.95M},
                new Product {Name = "Soccer Ball", Price = 19.50M},
                new Product {Name = "Corner Flag", Price = 34.95M}
            };

            decimal priceFilterTotal = productArray.Filter(p => (p?.Price ?? 0) >= 20).TotalPrices();
            decimal nameFilterTotal = productArray.Filter(p => (p?.Name?[0] == 'S')).TotalPrices();

            return View("Index", new string[]
            {
                $"Price Total: {priceFilterTotal:C2}",
                $"Name Total: {nameFilterTotal:C2}"
            });
        }
        */

        /*
        //It's common in controllers to have a method (action) which contains a single statement that selects the data to display and the view to render using Lambda Expressions.
        public ViewResult Index()
        {
            //Get a collection of products and use a lambda expression to return a collection of just names
            //Note that a method body can be written as a lambda expression when the method consists of a single statement (GetProducts() returns array of Products in the Products model class at the moment)
            return View(Product.GetProducts().Select(p => p?.Name));
        }
        */

        //Show that the var keyword allows us to define a local variable without explicity specifying the variable type - type inference. We just ask the compiler to figure it out
        /*
        public ViewResult Index()
        {
            var names = new[]
            {
                "Kayak", "Lifejacket", "Soccer ball"
            };

            return View(names);
        }
        */

        //Using anonymous types- we can combine object initializers and anonymous types to create ad-hoc classes on the fly.
        /*
        public ViewResult Index()
        {
            var products = new[]
            {
                new {Name = "Kayak", Price = 275M},
                new {Name = "Lifejacket", Price = 48.95M},
                new {Name = "Soccer Ball", Price = 19.50M},
                new {Name = "Corner Flag", Price = 34.95M}
            };

            return View(products.Select(p => p?.Name));
        }
        */

        //Use async to return Task<ViewResult>, which tells MVC the action will return a Task which will produce a ViewResult when it completes. 
        /*
        public async Task<ViewResult> Index()
        {
            long? length = await MyAsyncMethods.GetPageLength();
            return View(new string[] { $"Length: {length}" });
        }
        */

        //Getting names - the LINQ call generates a list of strings which contain the hardcoded name of the variables we used in our object - price and name
        /*
        public ViewResult Index()
        {
            var products = new[]
            {
                new {Name = "Kayak", Price = 275M},
                new {Name = "Lifejacket", Price = 48.95M},
                new {Name = "Soccer Ball", Price = 19.50M},
                new {Name = "Corner Flag", Price = 34.95M}
            };

            return View(products.Select(p => $"Name: {p.Name}, Price: {p.Price}"));
        }
        */

        //To make the above more maintanable, we can use the nameof operator instead of explicitly saying "Name: {blah}, Price: {blah}.
        //nameof literally just gets "Name" or "Price", which is the name of the variables used inside the object
        public ViewResult Index()
        {
            var products = new[]
            {
                new {Name = "Kayak", Price = 275M},
                new {Name = "Lifejacket", Price = 48.95M},
                new {Name = "Soccer Ball", Price = 19.50M},
                new {Name = "Corner Flag", Price = 34.95M}
            };

            return View(products.Select(p => $"{nameof(p.Name)}: {p.Name}, {nameof(p.Price)}: {p.Price}"));
        }
    }
}