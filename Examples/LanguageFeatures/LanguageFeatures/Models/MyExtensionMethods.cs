﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LanguageFeatures.Models
{
    public static class MyExtensionMethods
    {
        //create extension methods (think if we did not have access to source code we are using in the ShoppingCart class)
        public static decimal TotalPrices(this IEnumerable<Product> products)
        {
            decimal total = 0;

            foreach(Product prod in products)
            {
                total += prod?.Price ?? 0;
            }

            return total;
        }

        //Filtering extension method
        //remember that yield keyword assembles a new collection of enumerable products which are greater than or equal to minimumPrice in this case and returns them
        public static IEnumerable<Product> FilterByPrice(this IEnumerable<Product> productEnum, decimal minimumPrice)
        {
            foreach(Product p in productEnum)
            {
                if((p?.Price ?? 0) >= minimumPrice)
                {
                    yield return p;
                }
            }
        }

        //Showing how we can do the same filter but the method is almost the same.. pointing out how using lambda expressions simplify this A TON (good god this is a lot of code to write)
        public static IEnumerable<Product> FilterbyName(this IEnumerable<Product> productEnum, char firstLetter)
        {
            foreach(Product p in productEnum)
            {
                if(p?.Name[0] == firstLetter)
                {
                    yield return p;
                }
            }
        }

        //Create generic filter extension method using delegates (still stuck on showing the old ways here)
        //Note that when doing this, we need to define the two delegate functions to pass to this generic Filter extension method
        public static IEnumerable<Product> Filter(this IEnumerable<Product> productEnum, Func<Product, bool> selector)
        {
            foreach(Product p in productEnum)
            {
                if (selector(p))
                {
                    yield return p;
                }
            }
        }
    }
}
