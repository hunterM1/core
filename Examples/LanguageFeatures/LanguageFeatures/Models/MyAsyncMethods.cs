﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace LanguageFeatures.Models
{
    public class MyAsyncMethods
    {
        //This class shows how we can work with tasks directly showing old syntax and new (async)

        /*
        public static Task<long?> GetPageLength()
        {
            HttpClient client = new HttpClient();
            var httpTask = client.GetAsync("http://apress.com");

            //we could do other things here while the request is performed

            return httpTask.ContinueWith((Task<HttpResponseMessage> antecedent) =>
            {
                return antecedent.Result.Content.Headers.ContentLength;
            });
        }
        */

        //show how we can use async and await keywords to simplify the code
        //Await keyword tells C# to wait here for the call to complete before proceeding to the next lines of code. Must add the async keyword to the method signature
        //when you use await inside it. A Task<T> object is still returned as above.
        public async static Task<long?> GetPageLength()
        {
            HttpClient client = new HttpClient();
            var httpMessage = await client.GetAsync("http://apress.com");
            //do stuff
            return httpMessage.Content.Headers.ContentLength;
        }
    }
}
