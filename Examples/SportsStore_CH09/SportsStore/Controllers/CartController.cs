﻿using Microsoft.AspNetCore.Mvc;
using SportsStore.Infrastructure;
using SportsStore.Models;
using SportsStore.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Controllers
{
    public class CartController : Controller
    {
        private IProductRepository prodRepo;
        
        public CartController(IProductRepository prodRepo)
        {
            this.prodRepo = prodRepo;
        }

        public RedirectToActionResult AddToCart(int productId, string ReturnURL)
        {
            Product prod = prodRepo.Products.FirstOrDefault(p => p.ProductID == productId);

            if(prod != null)
            {
                Cart cart = GetCart();
                cart.AddItem(prod, 1);
                SaveCart(cart);
            }

            return RedirectToAction("Index", new { ReturnURL });
        }

        public RedirectToActionResult RemoveFromCart(int productId, string returnURL)
        {
            Product prod = prodRepo.Products.FirstOrDefault(p => p.ProductID == productId);

            if(prod != null)
            {
                Cart cart = GetCart();
                cart.RemoveLine(prod);
                SaveCart(cart);
            }

            return RedirectToAction("Index", new { returnURL });
        }

        public IActionResult Index(string returnURL)
        {
            CartIndexViewModel model = new CartIndexViewModel()
            {
                Cart = GetCart(),
                ReturnUrl = returnURL
            };

            return View(model);
        }

        private Cart GetCart()
        {
            Cart cart = HttpContext.Session.GetJson<Cart>("Cart") ?? new Cart();
            return cart;
        }

        private void SaveCart(Cart cart)
        {
            HttpContext.Session.SetJson("Cart", cart);
        }
    }
}
