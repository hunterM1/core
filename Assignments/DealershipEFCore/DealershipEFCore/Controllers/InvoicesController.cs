﻿using DealershipEFCore.Models;
using DealershipEFCore.Models.RepositoryInterfaces;
using DealershipEFCore.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Controllers
{
    public class InvoicesController : Controller
    {
        private IInvoiceRepository invoiceRepo;
        private int pageSize = 5;

        public InvoicesController(IInvoiceRepository invoiceRepo)
        {
            this.invoiceRepo = invoiceRepo;
        }
        public IActionResult List(int page = 1)
        {
            ViewBag.Title = "Invoices";
            ViewBag.PageAction = "List";

            IEnumerable<Invoice> allInvoices = invoiceRepo.Invoices
                .OrderBy(i => i.InvoiceID)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            PagingInfo pageInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = invoiceRepo.Invoices.Count()
            };

            InvoiceListViewModel vModel = new InvoiceListViewModel()
            {
                Invoices = allInvoices,
                PagingInfo = pageInfo
            };

            return View(vModel);
        }
    }
}
