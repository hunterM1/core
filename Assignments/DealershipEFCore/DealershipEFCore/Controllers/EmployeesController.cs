﻿using DealershipEFCore.Models;
using DealershipEFCore.Models.RepositoryInterfaces;
using DealershipEFCore.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Controllers
{
    public class EmployeesController : Controller
    {
        private IEmployeeRepository employeeRepo;
        private int pageSize = 5;

        public EmployeesController(IEmployeeRepository employeeRepo)
        {
            this.employeeRepo = employeeRepo;
        }

        public IActionResult Index()
        {
            ViewBag.Title = "Employee Info";
            return View();
        }
        public IActionResult ListEmployed(int page = 1)
        {
            ViewBag.Title = "Employee Info";
            ViewBag.Employed = "Currently Employed";
            ViewBag.PageAction = "ListEmployed";

            IEnumerable<Employee> employedEmps = employeeRepo.Employees
                .Where(e => e.IsCurrentlyEmployed == true)
                .OrderBy(e => e.EmployeeID)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            PagingInfo pageInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = employeeRepo.Employees.Where(e => e.IsCurrentlyEmployed == true).Count()
            };

            EmployeeListViewModel vModel = new EmployeeListViewModel()
            {
                Employees = employedEmps,
                PagingInfo = pageInfo
            };

            return View(vModel);
        }

        public IActionResult ListNotEmployed(int page = 1)
        {
            ViewBag.Title = "Employee Info";
            ViewBag.Employed = "Not Currently Employed";
            ViewBag.PageAction = "ListNotEmployed";

            IEnumerable<Employee> notEmployedEmps = employeeRepo.Employees
                .Where(e => e.IsCurrentlyEmployed == false)
                .OrderBy(e => e.EmployeeID)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            PagingInfo pageInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = employeeRepo.Employees.Where(e => e.IsCurrentlyEmployed == false).Count()
            };

            EmployeeListViewModel vModel = new EmployeeListViewModel()
            {
                Employees = notEmployedEmps,
                PagingInfo = pageInfo
            };

            return View(vModel);
        }
    }
}
