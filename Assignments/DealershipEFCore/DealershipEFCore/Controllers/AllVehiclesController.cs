﻿using DealershipEFCore.Models;
using DealershipEFCore.Models.RepositoryInterfaces;
using DealershipEFCore.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Controllers
{
    public class AllVehiclesController : Controller
    {
        private IAllVehicleRepository vehicleRepo;
        private int pageSize = 10;

        public AllVehiclesController(IAllVehicleRepository vehicleRepo)
        {
            this.vehicleRepo = vehicleRepo;
        }
        public IActionResult List(int page = 1)
        {
            ViewBag.Title = "All Vehicles In Dealership";
            ViewBag.PageAction = "List";

            IEnumerable<Vehicle> allVehicles = vehicleRepo.Vehicles
                .OrderBy(v => v.VehicleID)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            PagingInfo pageInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = vehicleRepo.Vehicles.Count()
            };

            VehicleListViewModel vModel = new VehicleListViewModel()
            {
                Vehicles = allVehicles,
                PagingInfo = pageInfo
            };

            return View(vModel);
        }
    }
}
