﻿using DealershipEFCore.Models.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Models.EfRepos
{
    public class EFCustomerRepository : ICustomerRepository
    {
        private DealershipDbContext context;

        public IEnumerable<Customer> Customers => context.Customers;

        public EFCustomerRepository(DealershipDbContext context)
        {
            this.context = context;
        }
    }
}
