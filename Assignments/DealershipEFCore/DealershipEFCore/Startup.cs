using DealershipEFCore.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DealershipEFCore.Models.RepositoryInterfaces;
using DealershipEFCore.Models.EfRepos;

namespace DealershipEFCore
{
    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DealershipDbContext>(options => options.UseSqlServer(
                Configuration["ConnectionStrings:AzureDealershipConnString"]));

            services.AddTransient<IAllVehicleRepository, EFAllVehiclesRepository>();
            services.AddTransient<ISoldVehicleRepository, EFSoldVehicleRepository>();
            services.AddTransient<IEmployeeRepository, EFEmployeeRepository>();
            services.AddTransient<ICustomerRepository, EFCustomerRepository>();
            services.AddTransient<IInvoiceRepository, EFInvoiceRepository>();

            services.AddMvc(options => options.EnableEndpointRouting = false);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseStatusCodePages();
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "NotEmployed",
                    template: "Employees/NotCurrentlyEmployed/Page{page}",
                    defaults: new { Controller = "Employees", action = "ListNotEmployed" });

                routes.MapRoute(
                    name: "Employed",
                    template: "Employees/CurrentlyEmployed/Page{page}",
                    defaults: new { Controller = "Employees", action = "ListEmployed" });

                routes.MapRoute(
                    name: "Invoices",
                    template: "Invoices/Page{page}",
                    defaults: new { Controller = "Invoices", action = "List" });

                routes.MapRoute(
                    name: "Customers",
                    template: "Customers/Page{page}",
                    defaults: new { Controller = "Customers", action = "List" });

                routes.MapRoute(
                    name: "SoldVehicles",
                    template: "SoldVehicles/Page{page}",
                    defaults: new { Controller = "SoldVehicles", action = "List" });

                routes.MapRoute(
                    name: "AllVehicles",
                    template: "AllVehicles/Page{page}",
                    defaults: new { Controller = "AllVehicles", action = "List" });

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}");
            });
        }
    }
}
