﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DealershipEFCore.Migrations
{
    public partial class updateYearToSmallInt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Year",
                table: "Vehicles");

            migrationBuilder.AddColumn<short>(
                name: "Year",
                table: "Vehicles",
                type: "smallint",
                nullable: false);

            /* Unable to use this as datetime2 is incompatible with smallint. Note that rolling back to previous migration than this may result in loss of data/not work (Down method may not work due to incompatible datatype issue).
            migrationBuilder.AlterColumn<short>(
                name: "Year",
                table: "Vehicles",
                type: "smallint",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");
            */
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Year",
                table: "Vehicles",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(short),
                oldType: "smallint");
        }
    }
}
