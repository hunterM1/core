﻿using DealershipEFCore.Infrastructure;
using DealershipEFCore.Models;
using DealershipEFCore.Models.RepositoryInterfaces;
using DealershipEFCore.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Controllers
{
    public class AllVehiclesController : Controller
    {
        private IAllVehicleRepository vehicleRepo;
        private int pageSize = 10;

        public AllVehiclesController(IAllVehicleRepository vehicleRepo)
        {
            this.vehicleRepo = vehicleRepo;
        }
        public IActionResult List(int page = 1)
        {
            ViewBag.Title = "All Vehicles In Dealership";
            ViewBag.PageAction = "List";

            IEnumerable<Vehicle> allVehicles = vehicleRepo.Vehicles
                .OrderBy(v => v.VehicleID)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            //also check for vehicles in cart and remove them from list (if we have a session opened up)
            bool cartSessionOpened = (HttpContext.Session.Get("Cart") != null);
            if (cartSessionOpened)
            {
                try
                {
                    Cart cart = HttpContext.Session.GetJson<Cart>("Cart");
                    foreach (CartLine cLine in cart.Lines)
                    {
                        allVehicles = allVehicles.Where(v => v.VehicleID != cLine.Vehicle.VehicleID);
                    }
                }
                catch(Exception e)
                {
                    //TODO: Create middleware to log exceptions
                }
            }

            //make count of total items in case cart session has been opened
            int totalItems = 0;
            if (cartSessionOpened)
            {
                totalItems = vehicleRepo.Vehicles.Count() - allVehicles.Count();
            }
            else
            {
                totalItems = vehicleRepo.Vehicles.Count();
            }

            PagingInfo pageInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = totalItems
            };

            VehicleListViewModel vModel = new VehicleListViewModel()
            {
                Vehicles = allVehicles,
                PagingInfo = pageInfo
            };

            return View(vModel);
        }
    }
}
