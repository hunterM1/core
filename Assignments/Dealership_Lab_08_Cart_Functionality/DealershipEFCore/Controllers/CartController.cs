﻿using DealershipEFCore.Infrastructure;
using DealershipEFCore.Models;
using DealershipEFCore.Models.RepositoryInterfaces;
using DealershipEFCore.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Controllers
{
    public class CartController : Controller
    {
        private IAllVehicleRepository repo;

        public CartController(IAllVehicleRepository repo)
        {
            this.repo = repo;
        }

        public RedirectToActionResult AddToCart(int vehicleId, string ReturnURL)
        {
            Vehicle vehicle = repo.Vehicles.FirstOrDefault(v => v.VehicleID == vehicleId);

            if (vehicle != null)
            {
                Cart cart = GetCart();
                cart.AddItem(vehicle);
                SaveCart(cart);
            }

            return RedirectToAction("Index", new { ReturnURL });
        }

        public RedirectToActionResult RemoveFromCart(int vehicleId, string returnURL)
        {
            Vehicle vehicle = repo.Vehicles.FirstOrDefault(v => v.VehicleID == vehicleId);

            if (vehicle != null)
            {
                Cart cart = GetCart();
                cart.RemoveLine(vehicle);
                SaveCart(cart);
            }

            return RedirectToAction("Index", new { returnURL });
        }

        public IActionResult Index(string returnURL)
        {
            CartIndexViewModel model = new CartIndexViewModel()
            {
                Cart = GetCart(),
                ReturnUrl = returnURL
            };

            return View(model);
        }
        private Cart GetCart()
        {
            Cart cart = HttpContext.Session.GetJson<Cart>("Cart") ?? new Cart();
            return cart;
        }

        private void SaveCart(Cart cart)
        {
            HttpContext.Session.SetJson("Cart", cart);
        }
    }
}
