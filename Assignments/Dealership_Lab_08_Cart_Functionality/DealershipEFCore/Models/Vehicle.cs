﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Models
{
    public class Vehicle
    {
        public int VehicleID { get; set; }
        
        [Column(TypeName = "char")]
        [StringLength(17)]
        public string VIN { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }

        [Column(TypeName = "smallint")]
        public short Year { get; set; }
        public int? Miles { get; set; }
        public string Color { get; set; }

        [Column(TypeName = "Money")]
        public decimal? Price { get; set; }

        //associations
        public List<InvoiceDetail> InvoiceDetails { get; set; }

    }
}
