﻿using DealershipEFCore.Models.RepositoryInterfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Models.EfRepos
{
    public class EFInvoiceRepository : IInvoiceRepository
    {
        private DealershipDbContext context;

        public EFInvoiceRepository(DealershipDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<Invoice> Invoices => context.Invoices
            .Include(i => i.InvoiceDetails)
            .ThenInclude(invDet => invDet.Vehicle)
            .Include(i => i.Customer)
            .Include(i => i.Employee);
    }
}
