﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DealershipEFCore.Models.RepositoryInterfaces;

namespace DealershipEFCore.Models.EfRepos
{
    public class EFAllVehiclesRepository : IAllVehicleRepository
    {
        private DealershipDbContext context;

        public IEnumerable<Vehicle> Vehicles => context.Vehicles
            .Where(v => !v.InvoiceDetails.Any(invDet => invDet.VehicleID == v.VehicleID));

        public EFAllVehiclesRepository(DealershipDbContext context)
        {
            this.context = context;
        }
            
    }
}
