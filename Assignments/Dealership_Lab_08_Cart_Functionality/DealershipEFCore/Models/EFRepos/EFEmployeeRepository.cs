﻿using DealershipEFCore.Models.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Models.EfRepos
{
    public class EFEmployeeRepository : IEmployeeRepository
    {
        private DealershipDbContext context;

        public IEnumerable<Employee> Employees => context.Employees;

        public EFEmployeeRepository(DealershipDbContext context)
        {
            this.context = context;
        }
    }
}
