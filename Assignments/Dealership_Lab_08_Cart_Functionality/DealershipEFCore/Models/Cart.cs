﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Models
{
    public class Cart
    {
        private List<CartLine> lineCollection = new List<CartLine>();

        public virtual IEnumerable<CartLine> Lines => lineCollection;

        public virtual void AddItem(Vehicle vehicle)
        {
            CartLine line = lineCollection.Where(lc => lc.Vehicle.VehicleID == vehicle.VehicleID).FirstOrDefault();

            if(line == null)
            {
                CartLine lineItem = new CartLine()
                {
                    Vehicle = vehicle
                };

                lineCollection.Add(lineItem);
            }
        }

        public virtual void RemoveLine(Vehicle vehicle) => lineCollection.RemoveAll(lc => lc.Vehicle.VehicleID == vehicle.VehicleID);

        public virtual decimal ComputeTotalValue() => (decimal)lineCollection.Sum(li => li.Vehicle.Price);

        public virtual void Clear() => lineCollection.Clear();
    }
}
