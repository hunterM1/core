﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Models
{
    public class Employee : Person
    {
        public int EmployeeID { get; set; }
        public DateTime? StartDate { get; set; }

        [Column(TypeName = "Money")]
        public decimal? Salary { get; set; }

        public bool IsCurrentlyEmployed { get; set; }


    }
}
