﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Models
{
    public class CartLine
    {
        public int CartLineID { get; set; }
        public Vehicle Vehicle { get; set; }
    }
}
