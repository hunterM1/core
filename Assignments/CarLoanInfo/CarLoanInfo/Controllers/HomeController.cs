﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CarLoanInfo.Models;

namespace CarLoanInfo.Controllers
{
    public class HomeController : Controller
    {
        /*
        public ViewResult Index()
        {
            
            LoanOfficer[] loanOfficerData = new LoanOfficer[]
            {
                new LoanOfficer {OfficerId = 1, Name = "Greg Steele"},
                new LoanOfficer {OfficerId = 2, Name = "Hunter Miller"},
                new LoanOfficer {OfficerId = 3, Name = "Caden Johnson"}
            };

            CarLoan[] carLoanData = new CarLoan[]
            {
                new CarLoan{LoanId = 1, InitialLoanAmt=10000M, Term = 48, Rate = 6, LoanStartDate = new DateTime(2017, 1, 20), CustomerName = "Tim Miller", IssuingLoanOfficer = loanOfficerData[0]},
                new CarLoan{LoanId = 2, InitialLoanAmt=22500M, Term = 48, Rate = 3.2, LoanStartDate = new DateTime(2021, 1, 12), CustomerName = "Caden Hoag", IssuingLoanOfficer = loanOfficerData[1]},
                new CarLoan{LoanId = 3, InitialLoanAmt=8250M, Term = 72, Rate = 4.3, LoanStartDate = new DateTime(2010, 12, 20), CustomerName = "Alex Cortez", IssuingLoanOfficer = loanOfficerData[2]},
                new CarLoan{LoanId = 4, InitialLoanAmt=14500M, Term = 48, Rate = 2.3, LoanStartDate = new DateTime(2021, 2, 11), CustomerName = "Bob White", IssuingLoanOfficer = loanOfficerData[0]}
            };

            var loanData = new LoanDataViewModel()
            {
                carLoans = carLoanData.ToList(),
                loanOfficers = loanOfficerData.ToList()
            };

            ViewBag.CurrentAvgLoanRate = 2.4;

            return View(loanData);
        }
        */
        public IData Data = LoanDataViewModel.SharedData;
        //public LoanDataViewModel Data = new LoanDataViewModel(LoanData.carLoanData, LoanData.loanOfficerData);

        public IActionResult Index()
        {
            ViewBag.CurrentAvgLoanRate = 3.8;
            return View(Data.CarLoans);
        }
    }
}