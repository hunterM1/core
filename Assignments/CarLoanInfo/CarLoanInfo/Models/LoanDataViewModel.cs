﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLoanInfo.Models
{
    public class LoanDataViewModel : IData
    {
        private static LoanDataViewModel sharedData = new LoanDataViewModel();
        private Dictionary<int, CarLoan> carLoans = new Dictionary<int, CarLoan>();

        public static LoanDataViewModel SharedData => sharedData;

        public LoanDataViewModel()
        {
            foreach(CarLoan c in LoanData.carLoanData)
            {
                AddCarLoan(c);
            }
        }

        public IEnumerable<CarLoan> CarLoans => carLoans.Values;

        public void AddCarLoan(CarLoan c) => carLoans.Add(c.LoanId, c);

    }
}
