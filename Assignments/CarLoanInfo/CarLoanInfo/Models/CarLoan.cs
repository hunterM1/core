﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLoanInfo.Models
{
    public class CarLoan
    {
        public int LoanId { get; set; }
        public decimal InitialLoanAmt { get; set; }
        public int Term { get; set; } //number of months
        public double Rate { get; set; }
        public DateTime LoanStartDate { get; set; }
        public string CustomerName { get; set; }
        public LoanOfficer IssuingLoanOfficer { get; set; }

        public DateTime GetLoanEndDate()
        {
            return LoanStartDate.AddMonths(Term);
        }

        //we are assuming monthly payments on a car loan here
        public decimal GetMonthlyPayment()
        {
            double rate = ((Rate / 100) / 12);
            decimal principal = InitialLoanAmt;
            int numOfMonths = Term;
            
            decimal upperEquation = (decimal)(rate * Math.Pow((1 + rate), numOfMonths));
            decimal lowerEquation = (decimal)(Math.Pow((1 + rate), numOfMonths) - 1);
            return principal * (upperEquation / lowerEquation);
        } 

        public decimal GetTotalLoanAmt()
        {
            return GetMonthlyPayment() * Term;
        }

        public decimal GetBankProfit()
        {
            return this.GetTotalLoanAmt() - this.InitialLoanAmt; 
        }

        public bool loanIsDone()
        {
            return GetLoanEndDate() < DateTime.Today;
        }
    }
}
