﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLoanInfo.Models
{
    public interface IData
    {
        IEnumerable<CarLoan> CarLoans { get; }
    }
}
