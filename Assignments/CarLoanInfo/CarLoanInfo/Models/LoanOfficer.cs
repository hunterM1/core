﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLoanInfo.Models
{
    public class LoanOfficer
    {
        public int OfficerId { get; set; }
        public string Name { get; set; }
    }
}
