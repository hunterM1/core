﻿using System;
using System.Collections.Generic;
using System.Text;
using CarLoanInfo.Models;
using Xunit;

namespace CarLoanInfo.Tests
{
    public class CarLoanCalculationTests
    {
        private static readonly LoanOfficer[] loanOfficerData = new LoanOfficer[]
        {
                new LoanOfficer {OfficerId = 1, Name = "Greg Steele"},
                new LoanOfficer {OfficerId = 2, Name = "Hunter Miller"},
                new LoanOfficer {OfficerId = 3, Name = "Caden Johnson"}
        };

        private static readonly CarLoan[] carLoanData = new CarLoan[]
        {
                new CarLoan{LoanId = 1, InitialLoanAmt=10000M, Term = 48, Rate = 6, LoanStartDate = new DateTime(2017, 1, 20), CustomerName = "Tim Miller", IssuingLoanOfficer = loanOfficerData[0]},
                new CarLoan{LoanId = 2, InitialLoanAmt=22500M, Term = 48, Rate = 3.2, LoanStartDate = new DateTime(2021, 1, 12), CustomerName = "Caden Hoag", IssuingLoanOfficer = loanOfficerData[1]},
                new CarLoan{LoanId = 3, InitialLoanAmt=8250M, Term = 72, Rate = 4.3, LoanStartDate = new DateTime(2010, 12, 20), CustomerName = "Alex Cortez", IssuingLoanOfficer = loanOfficerData[2]},
                new CarLoan{LoanId = 4, InitialLoanAmt=14500M, Term = 48, Rate = 2.3, LoanStartDate = new DateTime(2021, 2, 11), CustomerName = "Bob White", IssuingLoanOfficer = loanOfficerData[0]}
        };
    
        //Test that getLoanEndDate works for all given datetimes (in order of CarLoan creation above)
        [Fact]
        public void TestLoanEndDate()
        {
            DateTime test1 = new DateTime(2021, 1, 20);
            Assert.Equal(test1, carLoanData[0].GetLoanEndDate());

            DateTime test2 = new DateTime(2025, 1, 12);
            Assert.Equal(test2, carLoanData[1].GetLoanEndDate());

            DateTime test3 = new DateTime(2016, 12, 20);
            Assert.Equal(test3, carLoanData[2].GetLoanEndDate());

            DateTime test4 = new DateTime(2025, 2, 11);
            Assert.Equal(test4, carLoanData[3].GetLoanEndDate());
        }

        //Test get monthly payment (formula found from https://www.wikihow.com/Calculate-Auto-Loan-Payments)
        //calculator to verify answers from https://www.calculatorsoup.com/calculators/financial/
        [Fact]
        public void TestGetMonthlyPayment()
        {
            Assert.Equal(234.85M, Math.Round(carLoanData[0].GetMonthlyPayment(), 2));

            Assert.Equal(500.01M, Math.Round(carLoanData[1].GetMonthlyPayment(), 2));

            Assert.Equal(130.20M, Math.Round(carLoanData[2].GetMonthlyPayment(), 2));

            Assert.Equal(316.48M, Math.Round(carLoanData[3].GetMonthlyPayment(), 2));
        }

        [Fact]
        public void TestGetTotalLoanAmt()
        {
            Assert.Equal(11272.81M, Math.Round(carLoanData[0].GetTotalLoanAmt(), 2));

            Assert.Equal(24000.66M, Math.Round(carLoanData[1].GetTotalLoanAmt(), 2));

            Assert.Equal(9374.65M, Math.Round(carLoanData[2].GetTotalLoanAmt(), 2));

            Assert.Equal(15191.11M, Math.Round(carLoanData[3].GetTotalLoanAmt(), 2));
        }

        [Fact]
        public void TestGetBankProfit()
        {
            Assert.Equal(1272.81M, Math.Round(carLoanData[0].GetBankProfit(), 2));

            Assert.Equal(1500.66M, Math.Round(carLoanData[1].GetBankProfit(), 2));

            Assert.Equal(1124.65M, Math.Round(carLoanData[2].GetBankProfit(), 2));

            Assert.Equal(691.11M, Math.Round(carLoanData[3].GetBankProfit(), 2));
        }

        [Fact]
        public void TestLoanIsDone()
        {
            Assert.True(carLoanData[0].loanIsDone());

            Assert.False(carLoanData[1].loanIsDone());

            Assert.True(carLoanData[2].loanIsDone());

            Assert.False(carLoanData[3].loanIsDone());
        }
    }
}
