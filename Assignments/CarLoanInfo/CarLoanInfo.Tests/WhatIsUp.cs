﻿using CarLoanInfo.Controllers;
using CarLoanInfo.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarLoanInfo.Tests
{
    class WhatIsUp
    {
        public static void Main()
        {
            HomeController controller = new HomeController();
            controller.Data = new LoanDataViewModelFakeRepo();

            ViewResult result = (ViewResult)controller.Index();

            IEnumerable<CarLoan> carLoans = (IEnumerable<CarLoan>)result?.ViewData.Model;

            Comparer.Get<CarLoan>((c1, c2) => c1.LoanId == c2.LoanId && c1.InitialLoanAmt == c2.InitialLoanAmt && c1.Term == c2.Term && c1.Rate == c2.Rate && c1.GetMonthlyPayment() == c2.GetMonthlyPayment() && c1.GetTotalLoanAmt() == c2.GetTotalLoanAmt() && c1.GetBankProfit() == c2.GetBankProfit() && c1.LoanStartDate == c2.LoanStartDate && c1.GetLoanEndDate() == c2.GetLoanEndDate() && c1.IssuingLoanOfficer == c2.IssuingLoanOfficer && c1.loanIsDone() == c2.loanIsDone());
        }
    }
}
