﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CarLoanInfo.Models;

namespace CarLoanInfo.Tests
{
    public class LoanDataViewModelFakeRepo : IData
    {
        public static LoanOfficer[] loanOfficerData = new LoanOfficer[]
        {
                new LoanOfficer {OfficerId = 1, Name = "Test one"},
                new LoanOfficer {OfficerId = 2, Name = "Hunter Miller"},
                new LoanOfficer {OfficerId = 3, Name = "Test 3"}
        };

        public static CarLoan[] carLoanData = new CarLoan[]
        {
                new CarLoan{LoanId = 1, InitialLoanAmt=14830M, Term = 36, Rate = 6.2, LoanStartDate = new DateTime(2016, 1, 21), CustomerName = "Tim Miller", IssuingLoanOfficer = loanOfficerData[1]},
                new CarLoan{LoanId = 2, InitialLoanAmt=520M, Term = 72, Rate = 10.2, LoanStartDate = new DateTime(2010, 1, 12), CustomerName = "Caden Hoag", IssuingLoanOfficer = loanOfficerData[1]},
                new CarLoan{LoanId = 3, InitialLoanAmt=8250M, Term = 72, Rate = 4.3, LoanStartDate = new DateTime(2010, 12, 20), CustomerName = "Alex Cortez", IssuingLoanOfficer = loanOfficerData[2]},
                new CarLoan{LoanId = 4, InitialLoanAmt=14500M, Term = 48, Rate = 2.3, LoanStartDate = new DateTime(2021, 2, 11), CustomerName = "Bob White", IssuingLoanOfficer = loanOfficerData[0]}
        };
        public IEnumerable<CarLoan> CarLoans { get; } = LoanData.carLoanData;
    }
}
