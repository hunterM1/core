﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Models.ViewModels
{
    public class VINSearchViewModel
    {
        [StringLength(17, MinimumLength = 17, ErrorMessage = "VIN must be 17 characters long")]
        [Required(ErrorMessage = "Please specify the VIN of the vehicle to search")]
        public string VINToSearch { get; set; }

        public Vehicle Vehicle { get; set; }

        public string VehicleInfoString { get; set; }
    }
}
