﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Models.ViewModels
{
    public class CreateUserModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [UIHint("password")]
        public string Password { get; set; }

        [Required]
        [UIHint("password")]
        [Compare("Password", ErrorMessage = "The entered passwords do not match")]
        public string PasswordConfirmation { get; set; }

        public string ReturnUrl { get; set; }
    }
}
