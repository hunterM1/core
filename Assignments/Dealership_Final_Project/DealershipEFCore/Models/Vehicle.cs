﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Models
{
    public class Vehicle
    {
        [BindNever]
        public int VehicleID { get; set; }
        
        [Column(TypeName = "char")]
        [StringLength(17, MinimumLength = 17, ErrorMessage = "VIN must be 17 characters long")]
        [Required(ErrorMessage = "Please specify the VIN of the vehicle")]
        public string VIN { get; set; }
        [Required(ErrorMessage = "Please specify the make of the vehicle")]
        public string Make { get; set; }
        [Required(ErrorMessage = "Please specify the model of the vehicle")]
        public string Model { get; set; }

        [Column(TypeName = "smallint")]
        [Range(1900, 2022, ErrorMessage = "Please specify a correct vehicle year")]
        [Required(ErrorMessage = "Please specify the model year of the vehicle")]
        public short Year { get; set; }
        [Required]
        [Range(0, 2000000, ErrorMessage = "Please specify correct mileage of vehicle")]
        public int? Miles { get; set; }
        [Required(ErrorMessage = "Please specify the vehicle's color")]
        public string Color { get; set; }

        [Column(TypeName = "Money")]
        [Required(ErrorMessage = "Please specify the price of the vehicle")]
        [Range(0.01, 999999.99, ErrorMessage = "Please enter a positive dollar amount")]
        public decimal? Price { get; set; }

        //associations
        [BindNever]
        public List<InvoiceDetail> InvoiceDetails { get; set; }

    }
}
