﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DealershipEFCore.Models.RepositoryInterfaces;

namespace DealershipEFCore.Models.EfRepos
{
    public class EFAllVehiclesRepository : IAllVehicleRepository
    {
        private DealershipDbContext context;

        public IEnumerable<Vehicle> Vehicles => context.Vehicles
            .Where(v => !v.InvoiceDetails.Any(invDet => invDet.VehicleID == v.VehicleID));

        public EFAllVehiclesRepository(DealershipDbContext context)
        {
            this.context = context;
        }

        public void SaveVehicle(Vehicle vehicle)
        {
            if(vehicle.VehicleID == 0)
            {
                context.Vehicles.Add(vehicle);
            }
            else
            {
                Vehicle dbVehicle = context.Vehicles.FirstOrDefault(v => v.VehicleID == vehicle.VehicleID);
                if(dbVehicle != null)
                {
                    dbVehicle.VIN = vehicle.VIN;
                    dbVehicle.Make = vehicle.Make;
                    dbVehicle.Model = vehicle.Model;
                    dbVehicle.Year = vehicle.Year;
                    dbVehicle.Miles = vehicle.Miles;
                    dbVehicle.Color = vehicle.Color;
                    dbVehicle.Price = vehicle.Price;
                }
            }

            context.SaveChanges();
        }

        public Vehicle DeleteVehicle(int vehicleId)
        {
            Vehicle vehicle = context.Vehicles.FirstOrDefault(v => v.VehicleID == vehicleId);
            if(vehicle != null)
            {
                context.Vehicles.Remove(vehicle);
                context.SaveChanges();
            }

            return vehicle;
        }
            
    }
}
