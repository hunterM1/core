﻿using DealershipEFCore.Models.RepositoryInterfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Models.EfRepos
{
    public class EFSoldVehicleRepository : ISoldVehicleRepository
    {
        private DealershipDbContext context;

        public IEnumerable<Vehicle> Vehicles => context.Vehicles
            .Where(v => v.InvoiceDetails.Any(invDet => invDet.VehicleID == v.VehicleID))
            .Include(v => v.InvoiceDetails)
            .ThenInclude(invDet => invDet.Invoice)
            .ThenInclude(inv => inv.Customer)
            .Include(v => v.InvoiceDetails)
            .ThenInclude(invDet => invDet.Invoice)
            .ThenInclude(inv => inv.Employee);

        public EFSoldVehicleRepository(DealershipDbContext context)
        {
            this.context = context;
        }
    }
}
