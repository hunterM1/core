﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Models.RepositoryInterfaces
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> Employees { get; }
    }
}
