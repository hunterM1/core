﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Components
{
    public class AdminNavMenuViewComponent : ViewComponent
    {
        private List<NavigationMenuItem> adminNavItems = new List<NavigationMenuItem>();

        public AdminNavMenuViewComponent()
        {
            adminNavItems.AddRange(new List<NavigationMenuItem>
                {
                    new NavigationMenuItem
                    {
                        Controller = "Employees",
                        Action = "EmployeeIndex",
                        Value = "Employees"
                    },
                    new NavigationMenuItem
                    {
                        Controller = "Customers",
                        Action = "AllCustomersList",
                        Value = "Customer List",
                        PageRoute = 1
                    },
                    new NavigationMenuItem
                    {
                        Controller = "Invoices",
                        Action = "List",
                        Value = "Invoice List",
                        PageRoute = 1
                    },
                    new NavigationMenuItem
                    {
                        Controller = "Admin",
                        Action = "InvoicesNotShippedList",
                        Value = "Review Invoices with Vehicles Not Shipped",
                        PageRoute = 1
                    },
                    new NavigationMenuItem
                    {
                        Controller = "SoldVehicles",
                        Action = "SoldVehicleList",
                        Value = "Vehicles Sold",
                        PageRoute = 1
                    },
                    new NavigationMenuItem
                    {
                        Controller = "Admin",
                        Action = "VehicleList",
                        Value = "Add/Edit/Delete Vehicles",
                        PageRoute = 1
                    },
                    new NavigationMenuItem
                    {
                        Controller = "Admin",
                        Action = "GetVINInfo",
                        Value = "Get information from VIN"
                    }
                });
        }
        public IViewComponentResult Invoke()
        {
            ViewBag.SelectedAction = RouteData?.Values["Action"];
            ViewBag.SelectedController = RouteData?.Values["Controller"];

            return View(adminNavItems);
        }
    }
}
