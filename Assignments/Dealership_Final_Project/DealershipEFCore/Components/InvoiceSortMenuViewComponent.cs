﻿using DealershipEFCore.Models.RepositoryInterfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Components
{
    public class InvoiceSortMenuViewComponent : ViewComponent
    {
        private IInvoiceRepository repo;

        public InvoiceSortMenuViewComponent(IInvoiceRepository repo)
        {
            this.repo = repo;
        }

        public IViewComponentResult Invoke()
        {
            ViewBag.SelectedSalesPerson = RouteData?.Values["salespersonId"];

            ViewBag.SelectedController = RouteData?.Values["Controller"];

            return View(repo.Invoices
                .Select(i => i.Employee)
                .Distinct());
        }
    }
}
