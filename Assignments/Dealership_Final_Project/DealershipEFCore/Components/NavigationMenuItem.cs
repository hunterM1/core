﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Components
{
    public class NavigationMenuItem
    {
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Value { get; set; }
        public int? PageRoute { get; set; }
    }
}
