﻿using DealershipEFCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Infrastructure
{
    //VehicleIDComparer class used to check equality between 2 vehicles in database.
    sealed class VehicleIDComparer : EqualityComparer<Vehicle>
    {
        public override bool Equals(Vehicle v1, Vehicle v2) => v1.VehicleID == v2.VehicleID;

        public override int GetHashCode(Vehicle v) => v.GetHashCode();
    }
}
