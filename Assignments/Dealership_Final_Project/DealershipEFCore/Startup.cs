using DealershipEFCore.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DealershipEFCore.Models.RepositoryInterfaces;
using DealershipEFCore.Models.EfRepos;
using DealershipEFCore.Controllers;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.Identity;

namespace DealershipEFCore
{
    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DealershipDbContext>(options => options.UseSqlServer(
                Configuration["ConnectionStrings:LocalDealershipConnString"]));

            services.AddDbContext<AppIdentityDbContext>(options => options.UseSqlServer(
                Configuration["ConnectionStrings:LocalIdentityConnString"]));

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<AppIdentityDbContext>()
                .AddDefaultTokenProviders();

            services.AddTransient<IAllVehicleRepository, EFAllVehiclesRepository>();
            services.AddTransient<ISoldVehicleRepository, EFSoldVehicleRepository>();
            services.AddTransient<IEmployeeRepository, EFEmployeeRepository>();
            services.AddTransient<ICustomerRepository, EFCustomerRepository>();
            services.AddTransient<IInvoiceRepository, EFInvoiceRepository>();

            services.AddMvc(options => options.EnableEndpointRouting = false);

            services.AddScoped<Cart>(sp => SessionCart.GetCart(sp));
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddMemoryCache();
            services.AddSession();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseStatusCodePages();
            app.UseStaticFiles();

            app.UseSession();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "AdminHome",
                    template: "Admin/",
                    defaults: new { Controller = "Admin", action = "Index" });

                routes.MapRoute(
                    name: "AdminFilteredInvoices",
                    template: "Admin/InvoicesNotShipped/EmployeedID_{salespersonId}/Page{page}",
                    defaults: new { Controller = "Admin", action = "InvoicesNotShippedList", page = 1 });

                routes.MapRoute(
                    name: "AdminReviewNotShippedInvoices",
                    template: "Admin/InvoicesNotShipped/Page{page}",
                    defaults: new { Controller = "Admin", action = "InvoicesNotShippedList", page = 1 });

                routes.MapRoute(
                    name: "AdminVehicleList",
                    template: "Admin/VehicleList/Page{page}",
                    defaults: new { Controller = "Admin", action = "VehicleList", page = 1 });

                routes.MapRoute(
                    name: "AdminEditVehicle",
                    template: "Admin/EditVehicle",
                    defaults: new { Controller = "Admin", action = "Edit" });

                routes.MapRoute(
                    name: "Cart",
                    template: "Cart/",
                    defaults: new { Controller = "Cart", action = "Index" });

                routes.MapRoute(
                    name: "ShowNotEmployed",
                    template: "Admin/Employees/NotCurrentlyEmployed/Page{page}",
                    defaults: new { Controller = "Employees", action = "ListNotEmployed" });

                routes.MapRoute(
                    name: "ShowEmployed",
                    template: "Admin/Employees/CurrentlyEmployed/Page{page}",
                    defaults: new { Controller = "Employees", action = "ListEmployed" });

                routes.MapRoute(
                    name: "Employees",
                    template: "Admin/Employees/",
                    defaults: new { Controller = "Employees", action = "EmployeeIndex" });

                routes.MapRoute(
                    name: "FilteredInvoices",
                    template: "Admin/Invoices/EmployeedID_{salespersonId}/Page{page}",
                    defaults: new { Controller = "Invoices", action = "List", page = 1 });

                routes.MapRoute(
                    name: "Invoices",
                    template: "Admin/Invoices/Page{page}",
                    defaults: new { Controller = "Invoices", action = "List" });

                routes.MapRoute(
                    name: "Customers",
                    template: "Admin/Customers/Page{page}",
                    defaults: new { Controller = "Customers", action = "AllCustomersList", page = 1 });

                routes.MapRoute(
                    name: "SoldVehiclesShipped",
                    template: "Admin/SoldVehicles/Shipped_{shipped}/Page{page}",
                    defaults: new { Controller = "SoldVehicles", action = "SoldVehicleList", shipped = true });

                routes.MapRoute(
                    name: "SoldVehiclesNotShipped",
                    template: "Admin/SoldVehicles/NotShipped_{shipped}/Page{page}",
                    defaults: new { Controller = "SoldVehicles", action = "SoldVehicleList", shipped = false });

                routes.MapRoute(
                    name: "AllVehicles",
                    template: "AllVehicles/Page{page}",
                    defaults: new { Controller = "AllVehicles", action = "List" });

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}");
            });
        }
    }
}
