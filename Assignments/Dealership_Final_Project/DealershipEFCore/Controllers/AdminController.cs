﻿using DealershipEFCore.Models;
using DealershipEFCore.Models.RepositoryInterfaces;
using DealershipEFCore.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DealershipEFCore.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private int pageSize = 5;

        private IInvoiceRepository invoiceRepo;
        private IAllVehicleRepository allVehicleRepo;

        public AdminController(IInvoiceRepository invoiceRepo, IAllVehicleRepository allVehicleRepo)
        {
            this.invoiceRepo = invoiceRepo;
            this.allVehicleRepo = allVehicleRepo;
        }

        public IActionResult Index()
        {
            ViewBag.Title = "Admin Index";
            return View();
        }

        public IActionResult InvoicesNotShippedList(int salespersonId, int page = 1)
        {
            ViewBag.Title = "Invoices Not Shipped";
            ViewBag.PageAction = "InvoicesNotShippedList";

            IEnumerable<Invoice> invoicesNotShipped = invoiceRepo.Invoices
                .Where(i => salespersonId == 0 || i.Employee.EmployeeID == salespersonId)
                .Where(i => !i.Shipped)
                .OrderBy(i => i.InvoiceID)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            PagingInfo pageInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = salespersonId == 0 ? invoiceRepo.Invoices.Where(i => !i.Shipped).Count() : invoiceRepo.Invoices.Where(i => i.EmployeeID == salespersonId).Where(i => !i.Shipped).Count()
            };

            InvoiceListViewModel vModel = new InvoiceListViewModel()
            {
                Invoices = invoicesNotShipped,
                PagingInfo = pageInfo,
                SalespersonID = salespersonId
            };

            return View(vModel);
        }

        [HttpPost]
        public IActionResult MarkShipped(int invoiceId)
        {
            Invoice invoice = invoiceRepo.Invoices.FirstOrDefault(i => i.InvoiceID == invoiceId);

            if(invoice != null)
            {
                invoice.Shipped = true;
                invoiceRepo.SaveInvoice(invoice);
                TempData["crudSuccess"] = $"Invoice ID: {invoice.InvoiceID} | Customer: {invoice.Customer.FirstName} {invoice.Customer.LastName} | Salesperson: {invoice.Employee.FirstName} {invoice.Employee.LastName} | Mark Shipped Successfully";
            }

            return RedirectToAction("InvoicesNotShippedList");
        }

        public IActionResult VehicleList(int page = 1)
        {
            //Note that this lists vehicles not in any invoices - should not delete vehicles that were already sold
            ViewBag.Title = "Edit/Add/Delete Vehicles";
            ViewBag.PageAction = "VehicleList";

            IEnumerable<Vehicle> allVehicles = allVehicleRepo.Vehicles
                .OrderBy(v => v.VehicleID)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            PagingInfo pageInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = allVehicleRepo.Vehicles.Count()
            };

            VehicleListViewModel vModel = new VehicleListViewModel()
            {
                Vehicles = allVehicles,
                PagingInfo = pageInfo
            };
            return View(vModel);
        }

        public IActionResult Edit(int vehicleId)
        {
            ViewBag.Title = "Edit Vehicle";
            ViewBag.PageAction = "Edit";
            return View(allVehicleRepo.Vehicles.FirstOrDefault(v => v.VehicleID == vehicleId));
        }

        [HttpPost]
        public IActionResult Edit(Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                allVehicleRepo.SaveVehicle(vehicle);
                TempData["crudSuccess"] = $"Vehicle ID: {vehicle.VehicleID} | VIN: {vehicle.VIN} | Make: {vehicle.Make} | Model: {vehicle.Model} | Year: {vehicle.Year} | Price: {vehicle.Price:C2} | Saved Successfully";
                return RedirectToAction("VehicleList");
            }
            else
            {
                return View(vehicle);
            }
        }


        public IActionResult Create()
        {
            return View("Edit", new Vehicle());
        }

        [HttpPost]
        public IActionResult Delete(int vehicleId)
        {
            Vehicle vehicle = allVehicleRepo.DeleteVehicle(vehicleId);

            if(vehicle != null)
            {
                TempData["crudSuccess"] = $"Vehicle ID: {vehicle.VehicleID} | VIN: {vehicle.VIN} | Make: {vehicle.Make} | Model: {vehicle.Model} | Year: {vehicle.Year} | Price: {vehicle.Price:C2} | Deleted Successfully";
            }

            return RedirectToAction("VehicleList");
        }

        public IActionResult GetVINInfo()
        {
            ViewBag.Title = "VIN Info Search";
            ViewBag.PageAction = "GetVINInfo";

            Vehicle v = new Vehicle();
            VINSearchViewModel vModel = new VINSearchViewModel
            {
                VINToSearch = "",
                Vehicle = v,
                VehicleInfoString = ""
            };
            return View(vModel);
        }

        [HttpPost]
        public async Task<IActionResult> GetVINInfoAsync(VINSearchViewModel searchResponse)
        {
            if (ModelState.IsValid)
            {
                var info = await getVINSearchInfo(searchResponse.VINToSearch);
                if(info == null)
                {
                    TempData["VinSearchError"] = "Error receiving vehicle information based on VIN provided";
                    searchResponse.VehicleInfoString = "";
                    return View(searchResponse);
                }
                else
                {
                    dynamic jsonResp = JObject.Parse(info);
                    string year = jsonResp.Results[9].Value;
                    string make = jsonResp.Results[6].Value;
                    string model = jsonResp.Results[8].Value;
                    string driveType = jsonResp.Results[49].Value;
                    string numOfCylinders = jsonResp.Results[68].Value;
                    string displacement = jsonResp.Results[71].Value;

                    string vehicleInfoStr = $"VIN: {searchResponse.VINToSearch}\n" +
                        $"Year: {year}\n" +
                        $"Make: {make}\n" +
                        $"Model: {model}\n" +
                        $"Drive Type: {driveType}\n" +
                        $"Number of Cylinders: {numOfCylinders}\n" +
                        $"Displacement: {displacement} L";

                    if(year == null && make == null && model == null)
                    {
                        TempData["VinSearchError"] = "Error receiving vehicle information based on VIN provided";
                        searchResponse.VehicleInfoString = "";
                        return View(searchResponse);
                    }
                    else
                    {
                        searchResponse.VehicleInfoString = vehicleInfoStr;
                        searchResponse.Vehicle = new Vehicle();
                        try
                        {
                            searchResponse.Vehicle.Year = short.Parse(year);
                        }
                        catch(Exception)
                        {
                            //could not find a valid year from api call
                        }
                        searchResponse.Vehicle.Make = make;
                        searchResponse.Vehicle.Model = model;
                        searchResponse.Vehicle.VIN = searchResponse.VINToSearch;
                    }

                    return View(searchResponse);
                }

            }
            else
            {
                return View(searchResponse);
            }
        }

        //Get response from nhtsa.gov. If we have a status code that is not successful, return null so we can show user that we had an issue receiving info.
        private async Task<string> getVINSearchInfo(string vin)
        {
            var client = getClient();
            HttpResponseMessage response = await client.GetAsync($"{vin}?format=json");

            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                return content;
            }
            else
            {
                return null;
            }
        }

        private static HttpClient getClient()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://vpic.nhtsa.dot.gov/api/vehicles/decodevin/");
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
    }
}
