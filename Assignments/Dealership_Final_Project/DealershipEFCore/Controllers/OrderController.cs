﻿using DealershipEFCore.Models;
using DealershipEFCore.Models.RepositoryInterfaces;
using DealershipEFCore.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Controllers
{
    public class OrderController : Controller
    {
        private IInvoiceRepository invoiceRepo;
        private IEmployeeRepository employeeRepo;
        private Cart cart;

        public OrderController(IInvoiceRepository invoiceRepo, IEmployeeRepository employeeRepo, Cart cart)
        {
            this.invoiceRepo = invoiceRepo;
            this.employeeRepo = employeeRepo;
            this.cart = cart;
        }

        public IActionResult Checkout()
        {

            CheckoutViewModel vModel = new CheckoutViewModel()
            {
                Invoice = new Invoice(),
                EmployeeListItem = getEmployeeSelectListItems(),
                Cart = cart
            };

            return View(vModel);
        }


        [HttpPost]
        public IActionResult Checkout(CheckoutViewModel checkoutResp)
        {
            if(cart.Lines.Count() == 0)
            {
                ModelState.AddModelError("", "Sorry, your cart is empty!");
            }

            if (ModelState.IsValid)
            {
                checkoutResp.Invoice.InvoiceDetails = new List<InvoiceDetail>();
                foreach(CartLine cLine in cart.Lines)
                {
                    checkoutResp.Invoice.InvoiceDetails.Add(new InvoiceDetail()
                    {
                        InvoiceDetailID = 0,
                        InvoiceID = 0,
                        Invoice = checkoutResp.Invoice,
                        VehicleID = cLine.Vehicle.VehicleID,
                        Vehicle = cLine.Vehicle
                    });
                }
                checkoutResp.Invoice.EmployeeID = checkoutResp.SelectedEmployeeId;
                checkoutResp.Invoice.Employee = employeeRepo.Employees.Where(e => e.EmployeeID == checkoutResp.Invoice.EmployeeID).SingleOrDefault();

                invoiceRepo.SaveInvoice(checkoutResp.Invoice);
                return RedirectToAction("Completed");
            }
            else
            {
                checkoutResp.EmployeeListItem = getEmployeeSelectListItems();
                return View(checkoutResp);
            }
        }

        public IActionResult Completed()
        {
            cart.Clear();
            return View();
        }

        private SelectList getEmployeeSelectListItems()
        {
            var employees = employeeRepo.Employees.Select(e =>
                new SelectListItem
                {
                    Value = e.EmployeeID.ToString(),
                    Text = e.FirstName + " " + e.LastName
                });

            return new SelectList(employees, "Value", "Text");
        }
    }
}
