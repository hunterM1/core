﻿using DealershipEFCore.Models;
using DealershipEFCore.Models.RepositoryInterfaces;
using DealershipEFCore.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Controllers
{
    public class SoldVehiclesController : Controller
    {
        private ISoldVehicleRepository vehicleRepo;
        private int pageSize = 10;

        public SoldVehiclesController(ISoldVehicleRepository vehicleRepo)
        {
            this.vehicleRepo = vehicleRepo;
        }
        public IActionResult List(int page = 1, bool shipped = false)
        {
            if (shipped)
            {
                ViewBag.Title = "Vehicles Sold | Shipped";
                ViewBag.ShippedStatus = "Shipped";
            }
            else
            {
                ViewBag.Title = "Vehicles Sold | Not Shipped";
                ViewBag.ShippedStatus = "Not Shipped";
            }
            ViewBag.PageAction = "List";


            IEnumerable<Vehicle> vehiclesSold = vehicleRepo.Vehicles
                    .Where(v => v.InvoiceDetails.Any(invDet => invDet.Invoice.Shipped == shipped))
                    .OrderBy(v => v.VehicleID)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize);


            PagingInfo pageInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = vehicleRepo.Vehicles.Where(v => v.InvoiceDetails.Any(invDet => invDet.Invoice.Shipped == shipped)).Count()
            };

            VehicleListViewModel vModel = new VehicleListViewModel()
            {
                Vehicles = vehiclesSold,
                PagingInfo = pageInfo
            };

            return View(vModel);
        }
    }
}
