﻿using DealershipEFCore.Models;
using DealershipEFCore.Models.RepositoryInterfaces;
using DealershipEFCore.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Controllers
{
    public class AdminController : Controller
    {
        private int pageSize = 5;

        private IInvoiceRepository invoiceRepo;
        private IAllVehicleRepository allVehicleRepo;

        public AdminController(IInvoiceRepository invoiceRepo, IAllVehicleRepository allVehicleRepo)
        {
            this.invoiceRepo = invoiceRepo;
            this.allVehicleRepo = allVehicleRepo;
        }
        public IActionResult Index()
        {
            ViewBag.Title = "Admin Index";
            return View();
        }

        public IActionResult InvoicesNotShippedList(int salespersonId, int page = 1)
        {
            ViewBag.Title = "Invoices Not Shipped";
            ViewBag.PageAction = "InvoicesNotShippedList";

            IEnumerable<Invoice> invoicesNotShipped = invoiceRepo.Invoices
                .Where(i => salespersonId == 0 || i.Employee.EmployeeID == salespersonId)
                .Where(i => !i.Shipped)
                .OrderBy(i => i.InvoiceID)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            PagingInfo pageInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = salespersonId == 0 ? invoiceRepo.Invoices.Where(i => !i.Shipped).Count() : invoiceRepo.Invoices.Where(i => i.EmployeeID == salespersonId).Where(i => !i.Shipped).Count()
            };

            InvoiceListViewModel vModel = new InvoiceListViewModel()
            {
                Invoices = invoicesNotShipped,
                PagingInfo = pageInfo,
                SalespersonID = salespersonId
            };

            return View(vModel);
        }

        [HttpPost]
        public IActionResult MarkShipped(int invoiceId)
        {
            Invoice invoice = invoiceRepo.Invoices.FirstOrDefault(i => i.InvoiceID == invoiceId);

            if(invoice != null)
            {
                invoice.Shipped = true;
                invoiceRepo.SaveInvoice(invoice);
                TempData["crudSuccess"] = $"Invoice ID: {invoice.InvoiceID} | Customer: {invoice.Customer.FirstName} {invoice.Customer.LastName} | Salesperson: {invoice.Employee.FirstName} {invoice.Employee.LastName} | Mark Shipped Successfully";
            }

            return RedirectToAction("InvoicesNotShippedList");
        }

        public IActionResult VehicleList(int page = 1)
        {
            //Note that this lists vehicles not in any invoices - should not delete vehicles that were already sold
            ViewBag.Title = "Edit/Add/Delete Vehicles";
            ViewBag.PageAction = "VehicleList";

            IEnumerable<Vehicle> allVehicles = allVehicleRepo.Vehicles
                .OrderBy(v => v.VehicleID)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            PagingInfo pageInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = allVehicleRepo.Vehicles.Count()
            };

            VehicleListViewModel vModel = new VehicleListViewModel()
            {
                Vehicles = allVehicles,
                PagingInfo = pageInfo
            };
            return View(vModel);
        }

        public IActionResult Edit(int vehicleId)
        {
            ViewBag.Title = "Edit Vehicle";
            ViewBag.PageAction = "Edit";
            return View(allVehicleRepo.Vehicles.FirstOrDefault(v => v.VehicleID == vehicleId));
        }

        [HttpPost]
        public IActionResult Edit(Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                allVehicleRepo.SaveVehicle(vehicle);
                TempData["crudSuccess"] = $"Vehicle ID: {vehicle.VehicleID} | VIN: {vehicle.VIN} | Make: {vehicle.Make} | Model: {vehicle.Model} | Year: {vehicle.Year} | Price: {vehicle.Price:C2} | Saved Successfully";
                return RedirectToAction("VehicleList");
            }
            else
            {
                return View(vehicle);
            }
        }

        public IActionResult Create()
        {
            return View("Edit", new Vehicle());
        }

        [HttpPost]
        public IActionResult Delete(int vehicleId)
        {
            Vehicle vehicle = allVehicleRepo.DeleteVehicle(vehicleId);

            if(vehicle != null)
            {
                TempData["crudSuccess"] = $"Vehicle ID: {vehicle.VehicleID} | VIN: {vehicle.VIN} | Make: {vehicle.Make} | Model: {vehicle.Model} | Year: {vehicle.Year} | Price: {vehicle.Price:C2} | Deleted Successfully";
            }

            return RedirectToAction("VehicleList");
        }
    }
}
