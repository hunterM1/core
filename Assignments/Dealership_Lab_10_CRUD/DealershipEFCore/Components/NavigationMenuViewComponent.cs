﻿using DealershipEFCore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Components
{
    public class NavigationMenuViewComponent : ViewComponent
    {
        private List<NavigationMenuItem> navItems = new List<NavigationMenuItem>();

        public NavigationMenuViewComponent()
        {
            navItems.AddRange( new List<NavigationMenuItem> 
                {
                    new NavigationMenuItem
                    {
                        Controller = "Admin",
                        Action = "Index",
                        Value = "Admin"
                    },
                    new NavigationMenuItem
                    {
                        Controller = "AllVehicles",
                        Action = "List",
                        Value = "All Vehicles In Dealership",
                        PageRoute = 1
                    },
                new NavigationMenuItem
                    {
                        Controller = "SoldVehicles",
                        Action = "List",
                        Value = "Vehicles Sold",
                        PageRoute = 1
                    },
                new NavigationMenuItem
                    {
                        Controller = "Employees",
                        Action = "Index",
                        Value = "Employees"
                    },
                new NavigationMenuItem
                    {
                        Controller = "Customers",
                        Action = "List",
                        Value = "Customer List",
                        PageRoute = 1
                    },
                new NavigationMenuItem
                    {
                        Controller = "Invoices",
                        Action = "List",
                        Value = "Invoice List",
                        PageRoute = 1
                    }
                });
        }
        public IViewComponentResult Invoke()
        {
            ViewBag.SelectedController = RouteData?.Values["Controller"];

            return View(navItems);
        }
    }
}
