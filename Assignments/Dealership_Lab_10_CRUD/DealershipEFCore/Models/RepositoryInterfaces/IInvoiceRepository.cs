﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Models.RepositoryInterfaces
{
    public interface IInvoiceRepository
    {
        IEnumerable<Invoice> Invoices { get; }

        void SaveInvoice(Invoice invoice);
    }
}
