﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Models.ViewModels
{
    public class CheckoutViewModel
    {
        public Invoice Invoice { get; set; }

        public int SelectedEmployeeId { get; set; }

        public Cart Cart { get; set; }

        public IEnumerable<SelectListItem> EmployeeListItem { get; set; }
    }
}
