﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Models
{
    public class InvoiceDetail
    {
        public int InvoiceDetailID { get; set; }

        //associations
        public int InvoiceID { get; set; }
        public Invoice Invoice { get; set; }
        public int VehicleID { get; set; }
        public Vehicle Vehicle { get; set; }

    }
}
