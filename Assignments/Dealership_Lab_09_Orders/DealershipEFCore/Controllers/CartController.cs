﻿using DealershipEFCore.Infrastructure;
using DealershipEFCore.Models;
using DealershipEFCore.Models.RepositoryInterfaces;
using DealershipEFCore.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Controllers
{
    public class CartController : Controller
    {
        private IAllVehicleRepository repo;
        private Cart cart;

        public CartController(IAllVehicleRepository repo, Cart cartService)
        {
            this.repo = repo;
            this.cart = cartService;
        }

        public RedirectToActionResult AddToCart(int vehicleId, string ReturnURL)
        {
            Vehicle vehicle = repo.Vehicles.FirstOrDefault(v => v.VehicleID == vehicleId);

            if (vehicle != null)
            {
                cart.AddItem(vehicle);
            }

            return RedirectToAction("Index", new { ReturnURL });
        }

        public RedirectToActionResult RemoveFromCart(int vehicleId, string returnURL)
        {
            Vehicle vehicle = repo.Vehicles.FirstOrDefault(v => v.VehicleID == vehicleId);

            if (vehicle != null)
            {
                cart.RemoveLine(vehicle);
            }

            return RedirectToAction("Index", new { returnURL });
        }

        public IActionResult Index(string returnURL)
        {
            CartIndexViewModel model = new CartIndexViewModel()
            {
                Cart = cart,
                ReturnUrl = returnURL
            };

            return View(model);
        }
    }
}
