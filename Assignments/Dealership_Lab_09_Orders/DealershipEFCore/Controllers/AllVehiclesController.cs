﻿using DealershipEFCore.Infrastructure;
using DealershipEFCore.Models;
using DealershipEFCore.Models.RepositoryInterfaces;
using DealershipEFCore.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Controllers
{
    public class AllVehiclesController : Controller
    {
        private IAllVehicleRepository vehicleRepo;
        private Cart cart;
        private int pageSize = 10;

        public AllVehiclesController(IAllVehicleRepository vehicleRepo, Cart cartService)
        {
            this.vehicleRepo = vehicleRepo;
            this.cart = cartService;
        }
        public IActionResult List(int page = 1)
        {
            ViewBag.Title = "All Vehicles In Dealership";
            ViewBag.PageAction = "List";

            //initialize the vehicles to be changed based on if this cart has any lines or not
            IEnumerable<Vehicle> allVehicles;

            //initialize vehiclesAddedToCart (if user adds vehicle to cart, do not show them in table)
            List<Vehicle> vehiclesAddedToCart = null;
            if(this.cart.Lines.Count() > 0)
            {
                vehiclesAddedToCart = new List<Vehicle>();
                foreach(CartLine cLine in this.cart.Lines)
                {
                    int vehicleId = cLine.Vehicle.VehicleID;
                    vehiclesAddedToCart.Add(vehicleRepo.Vehicles.Where(v => v.VehicleID == vehicleId).SingleOrDefault());
                }
                //allVehicles will show all of them Except those that were added to cart
                allVehicles = vehicleRepo.Vehicles
                    .Except(vehiclesAddedToCart, new VehicleIDComparer())
                    .OrderBy(v => v.VehicleID)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize);
            }
            else
            {
                allVehicles = vehicleRepo.Vehicles
                    .OrderBy(v => v.VehicleID)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize);
            }

            //make count of total vehicles for pagination. Do not show the ones that are in the cart
            int totalItems = 0;
            if (this.cart.Lines.Count() > 0)
            {
                totalItems = vehicleRepo.Vehicles
                    .Except(vehiclesAddedToCart, new VehicleIDComparer())
                    .Count();
            }
            else
            {
                totalItems = vehicleRepo.Vehicles.Count();
            }

            PagingInfo pageInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = totalItems
            };

            VehicleListViewModel vModel = new VehicleListViewModel()
            {
                Vehicles = allVehicles,
                PagingInfo = pageInfo
            };

            return View(vModel);
        }
    }
}
