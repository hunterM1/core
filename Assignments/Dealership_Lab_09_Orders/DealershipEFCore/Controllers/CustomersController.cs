﻿using DealershipEFCore.Models;
using DealershipEFCore.Models.RepositoryInterfaces;
using DealershipEFCore.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Controllers
{
    public class CustomersController : Controller
    {
        private ICustomerRepository customerRepo;
        private int pageSize = 10;

        public CustomersController(ICustomerRepository customerRepo)
        {
            this.customerRepo = customerRepo;
        }
        public IActionResult List(int page = 1)
        {
            ViewBag.Title = "Customers";
            ViewBag.PageAction = "List";

            IEnumerable<Customer> allCustomers = customerRepo.Customers
                .OrderBy(c => c.FirstName)
                .ThenBy(c => c.LastName)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            PagingInfo pageInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = customerRepo.Customers.Count()
            };

            CustomerListViewModel vModel = new CustomerListViewModel()
            {
                Customers = allCustomers,
                PagingInfo = pageInfo
            };

            return View(vModel);
        }
    }
}
