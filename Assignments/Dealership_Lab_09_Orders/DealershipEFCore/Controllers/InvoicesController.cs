﻿using DealershipEFCore.Models;
using DealershipEFCore.Models.RepositoryInterfaces;
using DealershipEFCore.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Controllers
{
    public class InvoicesController : Controller
    {
        private IInvoiceRepository invoiceRepo;
        private int pageSize = 1;

        public InvoicesController(IInvoiceRepository invoiceRepo)
        {
            this.invoiceRepo = invoiceRepo;
        }
        public IActionResult List(int salespersonId, int page = 1)
        {
            ViewBag.Title = "Invoices";
            ViewBag.PageAction = "List";

            IEnumerable<Invoice> allInvoices = invoiceRepo.Invoices
                .Where(i => salespersonId == 0 || i.Employee.EmployeeID == salespersonId)
                .OrderBy(i => i.InvoiceID)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            PagingInfo pageInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = salespersonId == 0 ? invoiceRepo.Invoices.Count() : invoiceRepo.Invoices.Where(i => i.EmployeeID == salespersonId).Count()
            };

            InvoiceListViewModel vModel = new InvoiceListViewModel()
            {
                Invoices = allInvoices,
                PagingInfo = pageInfo,
                SalespersonID = salespersonId
            };

            return View(vModel);
        }
    }
}
