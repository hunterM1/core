﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DealershipEFCore.Models
{
    public class Invoice
    {
        [BindNever]
        public int InvoiceID { get; set; }

        [BindNever]
        public DateTime InvoiceDate { get; set; }

        //associations
        [BindNever]
        public int CustomerID { get; set; }

        public Customer Customer { get; set; }

        [BindNever]
        public int EmployeeID { get; set; }

        public Employee Employee { get; set; }

        [BindNever]
        public List<InvoiceDetail> InvoiceDetails { get; set; }

    }
}
